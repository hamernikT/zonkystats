//
//  GraphViewModel.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation
import Charts

protocol GraphViewModeling {
    func getData(completion: @escaping (BarChartData)->())
}

class GraphViewModel: GraphViewModeling {
    
    private let networkService: GraphNetworkServicing
    
    init(networkService: GraphNetworkServicing) {
        self.networkService = networkService
    }
    
    func getData(completion: @escaping (BarChartData)->()) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let baseDate = dateFormatter.date(from: "2019-01-01") ?? Date()
        networkService.getLoanData(iteration: 0, baseDate: baseDate, data: nil) { [weak self] (data) in
            guard let strongSelf = self else { return }
            guard let data = data else { return }
            let sorted = data.sorted(by: { $0.0 < $1.0 })
            let months = sorted.map{ $0.key }
            let values = sorted.map { $0.value }
            completion(strongSelf.createChartData(months: months, values: values))
        }
    }
    
    private func createChartData(months: [Date], values: [String]) -> BarChartData {
        var entries: [BarChartDataEntry] = []
        
        for i in 0..<months.count {
            let dataEntry = BarChartDataEntry(x: Double(i+1), y: Double(values[i]) ?? 0.0)
            entries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(entries: entries, label: "Loans per month")
        let chartData = BarChartData(dataSet: chartDataSet)
        return chartData
    }
}

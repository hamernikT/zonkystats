//
//  MarketplaceRequest.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation
import Alamofire

class MarketplaceRequest: Request {
    var endpoint: ApiEndpoint = .marketplace
    var parameters: Parameters
    
    init(with startDateString: String, endDateString: String) {
        parameters = ["datePublished__gte": startDateString, "datePublished__lt": endDateString]
    }
}

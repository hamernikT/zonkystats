//
//  Request.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation
import Alamofire

protocol Request {
    var endpoint: ApiEndpoint { get }
    var method: HTTPMethod { get }
    var parameters: Parameters { get }
    var headers: HTTPHeaders { get }
    var encoding: ParameterEncoding { get }
}

extension Request {
    var method: HTTPMethod {
        return .get
    }
    var encoding: ParameterEncoding {
        return method == .get ? URLEncoding.default : JSONEncoding.default
    }
    var url: String {
        return endpoint.url
    }
    
    var headers: HTTPHeaders {
        return [:]
    }
    var parameters: Parameters {
        return [:]
    }
}

//
//  ApiEndpoint.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation

enum ApiEndpoint {
    
    var url: String {
        return "\(baseURL)\(rawValue)"
    }
    
    var baseURL: String {
        return "https://api.zonky.cz/"
    }
    
    case marketplace
    
    var rawValue: String {
        switch self {
        case .marketplace:
            return "loans/marketplace"
        }
    }
}

//
//  GraphNetworkService.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation
import Alamofire

protocol GraphNetworkServicing {
    func getLoanData(iteration: Int, baseDate: Date, data: [Date: String]?, completion: @escaping ([Date: String]?)->())
}

class GraphNetworkService: GraphNetworkServicing {
    
    // Methods here should in general accept only some request and define some result type in real life. It would mean to create a generic networking base layer
    func getLoanData(iteration: Int, baseDate: Date, data: [Date: String]?, completion: @escaping ([Date: String]?)->()) {
        
        let startDate = Calendar.current.date(byAdding: .month, value: iteration, to: baseDate) ?? Date()
        let endDate = Calendar.current.date(byAdding: .month, value: iteration+1, to: baseDate) ?? Date()
        
        let startString = startDate.apiDateString
        let endString = endDate.apiDateString
        
        let request = MarketplaceRequest(with: startString, endDateString: endString)
        
        Alamofire.request(request.url, method: request.method, parameters: request.parameters, encoding: request.encoding, headers: request.headers).responseJSON { [weak self] (response) in
            
            guard let strongSelf = self else { return }
            
            if let headers = response.response?.allHeaderFields as? [String: Any] {
                if let total = headers["x-total"] as? String {
                    var dataToReturn = data ?? [Date: String]()
                    dataToReturn[startDate] = total
                    
                    if iteration < 11 {
                        let i = iteration + 1
                        strongSelf.getLoanData(iteration: i, baseDate: baseDate, data: dataToReturn, completion: completion)
                    } else {
                        print(dataToReturn)
                        completion(dataToReturn)
                    }
                }
            }
        }
    }
}

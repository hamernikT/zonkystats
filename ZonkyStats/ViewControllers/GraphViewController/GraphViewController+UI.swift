//
//  GraphViewController+UI.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation
import SnapKit
import Charts

extension GraphViewController {
    func interfaceInit() {
        view.backgroundColor = .white
        setupGraphView()
    }
    
    private func setupGraphView() {
        let barChartView = BarChartView()
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.leftAxis.drawGridLinesEnabled = false
        view.addSubview(barChartView)
        barChartView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        
        self.barChartView = barChartView
    }
}

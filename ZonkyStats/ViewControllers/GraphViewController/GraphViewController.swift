//
//  GraphViewController.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import UIKit
import Charts

class GraphViewController: BaseViewController {
    
    weak var barChartView: BarChartView!
    
    private let viewModel: GraphViewModeling
    
    init(viewModel: GraphViewModeling) {
        self.viewModel = viewModel
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        interfaceInit()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.getData { [weak self] (barData) in
            self?.barChartView.data = barData
        }
    }
}


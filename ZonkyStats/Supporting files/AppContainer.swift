//
//  AppContainer.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import Foundation
import Swinject

infix operator ~ : ComparisonPrecedence

func ~ <Service>(lhs: Resolver, rhs: Service.Type) -> Service {
    return lhs.resolve(rhs)!
}

typealias GraphViewControllerFactory = () -> GraphViewController

class AppContainer {
    
    static let container = Container()
    
    class func mainRegister() {
        registerServices()
        registerFactories()
    }
    
    private class func registerServices() {
        
        // MARK: - Networking services
        container.register(GraphNetworkServicing.self) { r in
            GraphNetworkService()
        }
    }
    
    private class func registerFactories() {
        
        // MARK: - Register controller factories
        container.register(GraphViewControllerFactory.self) { r in
            return {
                let viewModel = GraphViewModel(networkService: r ~ GraphNetworkServicing.self)
                return GraphViewController(viewModel: viewModel)
            }
        }
    }
}

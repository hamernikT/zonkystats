//
//  AppDelegate.swift
//  ZonkyStats
//
//  Created by Thomas Hamerník on 07/07/2019.
//  Copyright © 2019 Zonky s.r.o. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        AppContainer.mainRegister()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let rootController = (AppContainer.container ~ GraphViewControllerFactory.self)()
        window?.rootViewController = rootController
        window?.makeKeyAndVisible()
        return true
    }
}


# README #

Hello to Zonky :)

I created a smiple test app, where user is presented with a bar graph of loans provided by Zonky for each month of this year. 

How to run application:
    If you don't have cocoapods {
        sudo gem cocoapods
    }
    pod install

Application is created as a simple MVVM. Application containter register Controller and Network service. This one controller has its injected ViewModel protocol. ViewModel has injected NetworkService protocol. This network service contains only one function which recursively call it self until some condition is fullfilled and then in completion block return the data. 

In real life project we would want to enhance this "network layer" with something like BaseNetworking (aka RxNetworking layer). Here would be handled connection and model / error parsing. This layer would provide some protocol with a method producing an Observable with generic response model. 

Hope this simple demonstration and description is adequate :)

Looking forward for your response 

Tom.


    
    
